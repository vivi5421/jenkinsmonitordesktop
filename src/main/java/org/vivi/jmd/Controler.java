package org.vivi.jmd;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.vivi.jmd.log.LogManager;
import org.vivi.jmd.log.Logger;
import org.vivi.jmd.notify.ControlerNotifier;
import org.vivi.jmd.notify.INotified;
import org.vivi.jmd.notify.INotifier;
import org.vivi.jmd.notify.NotifiedChanges;
import org.vivi.jmd.rss.model.Feed;
import org.vivi.jmd.rss.read.UpdateStatus;
import org.vivi.jmd.save.JenkinsMonitorDesktopSaver;
import org.vivi.jmd.save.model.SFeed;
import org.vivi.jmd.ui.rss.JFeed;
import org.vivi.jmd.util.TwoStrings;
import org.vivi.jmd.util.Utils;

public class Controler implements INotifier {

  private static final transient Logger sLogger = LogManager.getLogger(Controler.class);

  private static final long REFRESH_DELAY = 60000;

  List<Feed> feeds;

  ControlerNotifier notifier;

  NotifiedChanges changes;

  Timer timer;

  JenkinsMonitorDesktopSaver saver;

  public Controler(boolean isAutomaticRefreshEnabled) {
    feeds = new ArrayList<Feed>();
    notifier = new ControlerNotifier();
    changes = new NotifiedChanges();
    saver = new JenkinsMonitorDesktopSaver();
    if (isAutomaticRefreshEnabled) {
      activateAutomaticRefresh();
    }
  }

  public void init() {
    List<SFeed> sFeeds = saver.load(this);
    if (sFeeds != null) {
      if (sLogger.isInfoEnabled()) {
        sLogger.info("init controler with those feeds: " + sFeeds);
      }
      List<TwoStrings> listInputs = new ArrayList<TwoStrings>();
      for (SFeed sFeed : sFeeds) {
        listInputs.add(new TwoStrings(sFeed.title, sFeed.url));
      }
      addFeeds(listInputs, false);
    }
    else if (sLogger.isInfoEnabled()) {
      sLogger.info("Nothing to be loaded from saved config (or config not found.");
    }
  }

  public void updateFeeds() {
    updateFeeds(feeds);
  }

  public void updateFeeds(List<Feed> feeds) {
    if (sLogger.isDebugEnabled()) {
      sLogger.debug("Refresh feeds: " + feeds);
    }
    if (!feeds.isEmpty()) {
      for (Feed feed : feeds) {
        UpdateStatus status = feed.update();
        if (status == UpdateStatus.UpdatedNewStatus) {
          changes.addNewStatus(feed);
        }
        else if (status == UpdateStatus.UpdatedSameStatus) {
          changes.addUpdates(feed);
        }
        else if (status == UpdateStatus.Error) {
          changes.addError(feed);
        }
      }
      notifyChanges(changes);
    }
  }

  public void addFeed(String title, String url) {
    addFeeds(Arrays.asList(new TwoStrings(title, url)));
  }

  public void addFeeds(List<TwoStrings> datas) {
    addFeeds(datas, true);
  }

  private void addFeeds(List<TwoStrings> datas, boolean save) {
    if (sLogger.isInfoEnabled()) {
      sLogger.info("Adding new feeds: " + datas + ".");
    }
    if (datas != null && !datas.isEmpty()) {
      for (TwoStrings data : datas) {
        Feed newFeed;
        try {
          newFeed = createFeed(data.str1, data.str2);
          newFeed.update();
          feeds.add(newFeed);
          changes.addNewFeed(newFeed);
        }
        catch (MalformedURLException e) {
          sLogger.error("Cannot create feed: " + data.str1 + "\t" + data.str2 + ".\n" + e.getMessage());
        }
      }
      if (save) {
        saveOptions();
      }
      notifyChanges(changes);
    }
  }

  Feed createFeed(String title, String url) throws MalformedURLException {
    return new Feed(new URL(url), title);
  }

  public void removeItems(List<JFeed> jFeeds) {
    if (sLogger.isInfoEnabled()) {
      sLogger.info("Removing those feeds: " + jFeeds);
    }
    for (JFeed jFeed : jFeeds) {
      Feed feed = getRelatedFeed(jFeed);
      feeds.remove(feed);
      changes.addRemovedFeed(feed);
    }
    saveOptions();
    notifyChanges(changes);
  }

  Feed getRelatedFeed(JFeed jFeed) {
    for (Feed feed : feeds) {
      if (Utils.areEquals(jFeed.getTitle(), feed.getTitle()) && Utils.areEquals(jFeed.getLink(), feed.getLink())) {
        return feed;
      }
    }
    return null;
  }

  public void saveOptions() {
    try {
      saver.save(feeds);
    }
    catch (IOException e) {
      sLogger.error("Could not save options file: " + e.getMessage());
    }
  }

  @Override
  public void registerToNotifications(INotified notified) {
    notifier.registerToNotifications(notified);
  }

  @Override
  public void unregisterToNotifications(INotified notified) {
    notifier.unregisterToNotifications(notified);
  }

  void notifyChanges(NotifiedChanges changes) {
    notifier.notifyRegistereds(changes);
  }

  void activateAutomaticRefresh() {
    timer = new Timer();
    timer.schedule(new TimerTask() {

      @Override
      public void run() {
        if (sLogger.isInfoEnabled()) {
          sLogger.info("Automatic Refresh processing...");
          if (sLogger.isDebugEnabled()) {
            sLogger.debug("Refresh process called at: " + new Date().toGMTString());
          }
        }
        updateFeeds();
      }
    }, REFRESH_DELAY, REFRESH_DELAY);
  }
}

package org.vivi.jmd;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import org.vivi.jmd.rss.model.Feed;

public class MockControler extends Controler {
	
	public MockControler(boolean isAutomaticRefreshEnabled) {
		super(isAutomaticRefreshEnabled);
	}

	@Override
	Feed createFeed(String title, String url) throws MalformedURLException {
		/*
		 * This is required because of crappy code. When we want to add a mocked
		 * url, providing the filename only, we need to add the reference to the
		 * current project to let Java finding the file. But when we load a
		 * config, we already have the full url, so we don't add to add it
		 * again. This has to be fixed but is working fine in real conditions.
		 */
		// if we have some {@link java.io.File#separator} characters, we
		// consider we have folders, we take the last one to have only the
		// filename (with the extension).
		String serieName = null;
		int serieIndex = -1;
		if (url.contains(File.separator)) {
			String[] split = url.split(File.separator);
			url = split[split.length - 1];
			// Then we remove the extension since is added just after
			url = url.substring(0, url.length() - 4);
			if (url.contains("#")) {
				serieName = url.split("#")[0];
				serieIndex = Integer.valueOf(url.split("#")[1]);
			}
		}
		URL fileUrl = this.getClass().getResource(url + ".xml");
		return new Feed(fileUrl, title);
	}
}

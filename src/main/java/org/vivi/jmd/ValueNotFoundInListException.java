package org.vivi.jmd;

import java.util.List;

import org.vivi.jmd.util.Utils;

public class ValueNotFoundInListException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public <E> ValueNotFoundInListException(List<E> items, E lookForItem) {
		System.out.println(Utils.generString("Could not find this item [{0}] in this list [{1}]", lookForItem.toString(),
				items.toString()));
	}

}

package org.vivi.jmd.adapt;

import java.util.HashMap;
import java.util.Map;

import org.vivi.jmd.log.LogManager;
import org.vivi.jmd.log.Logger;
import org.vivi.jmd.rss.model.JenkinsStatus;
import org.vivi.jmd.ui.rss.JFeedMessageStatus;

/**
 * Class processing adapt process between a given JenkinsStatus and a
 * JFeedMessageStatus. The JenkinsStatus is the result of the build in a Jenkins
 * point of view/ success, failure, unstable. The JFeedMessageStatus is a
 * graphical representation of a status, meaning the associated picture.
 * 
 * @author vivi
 * 
 */
public class StatusAdaptor {

	private static final transient Logger sLogger = LogManager.getLogger(StatusAdaptor.class);

	private static final Map<JenkinsStatus, JFeedMessageStatus> MAP_STATUS;

	static {
		MAP_STATUS = new HashMap<JenkinsStatus, JFeedMessageStatus>();
		MAP_STATUS.put(JenkinsStatus.Success, JFeedMessageStatus.Blue);
		MAP_STATUS.put(JenkinsStatus.Unstable, JFeedMessageStatus.Yellow);
		MAP_STATUS.put(JenkinsStatus.Failure, JFeedMessageStatus.Red);
	}

	static <K, V> V lookInMap(Map<K, V> map, K key) {
		V result = null;
		if (map != null && map.containsKey(key)) {
			result = map.get(key);
		}
		if (sLogger.isDebugEnabled()) {
			sLogger.debug("lookInMap [" + key + "] into [" + map.toString() + "]. Found [" + result + "]");
		}
		return result;
	}

	static <K, V> K lookReverseInMap(Map<K, V> map, V value) {
		K result = null;
		if (map != null) {
			for (Map.Entry<K, V> entry : map.entrySet()) {
				if (entry.getValue().equals(value)) {
					result = entry.getKey();
				}
			}
		}
		if (sLogger.isDebugEnabled()) {
			sLogger.debug("lookReverseInMap [" + value + "] into [" + map.toString() + "]. Found [" + result + "].");
		}
		return result;
	}

	public static JFeedMessageStatus adaptStatus(JenkinsStatus status) {
		JFeedMessageStatus result = lookInMap(MAP_STATUS, status);
		if (sLogger.isDebugEnabled()) {
			sLogger.debug("Adapt status from " + status + " to " + result + ".");
		}
		return result;
	}

}

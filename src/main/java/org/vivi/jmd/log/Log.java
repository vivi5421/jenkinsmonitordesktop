package org.vivi.jmd.log;

import java.util.Date;

import org.vivi.jmd.util.Utils;

public class Log {

	int level;

	String log;

	Class clazz;
	
	Date date;

	public Log(int level, String str, Class clazz) {
		this.level = level;
		this.log = str;
		this.clazz = clazz;
		date = new Date();
	}

	public int getLevel() {
		return level;
	}

	public String getLog() {
		return log;
	}

	public Class getClazz() {
		return clazz;
	}

	public String getMessage() {
		String pattern = "{0} [{1}] {2}  {3}";
		return Utils.generString(pattern, date.toGMTString(), getLevel(level), clazz.getCanonicalName(), log);
	}

	String getLevel(int level) {
		String levelName;
		switch (level) {
		case 0:
			levelName = "ERROR";
			break;
		case 1:
			levelName = "WARN";
			break;
		case 2:
			levelName = "INFO";
			break;
		case 3:
			levelName = "DEBUG";
			break;
		default:
			levelName = "UNKNOWN";
		}
		return levelName;
	}

}

package org.vivi.jmd.log;

public class LogManager {
	
	static int currentLevel = 0;
	
	public static Logger getLogger(Class clazz) {
		return new Logger(currentLevel, clazz);
	}
	
	public static void setLevel(int level) {
		currentLevel = level;
	}

}

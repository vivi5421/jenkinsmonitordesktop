package org.vivi.jmd.log;

import java.util.ArrayList;
import java.util.List;

public class Logger {

	public static final int ERROR = 0;

	public static final int WARN = 1;

	public static final int INFO = 2;

	public static final int DEBUG = 3;

	int currentLevel;

	static List<ILogRegistered> registered;
	
	Class clazz;
	
	static List<Log> pooledLogs;
	
	static {
		registered = new ArrayList<ILogRegistered>();
		pooledLogs = new ArrayList<Log>();
	}

	public Logger(int level, Class clazz) {
		this.currentLevel = level;
		this.clazz = clazz;
	}

	public boolean isErrorEnabled() {
		return currentLevel >= ERROR;
	}

	public boolean isWarnEnabled() {
		return currentLevel >= WARN;
	}

	public boolean isInfoEnabled() {
		return currentLevel >= INFO;
	}

	public boolean isDebugEnabled() {
		return currentLevel >= DEBUG;
	}

	public void error(String str) {
		notify(generLog(ERROR, str));
	}

	public void warn(String str) {
		notify(generLog(WARN, str));
	}

	public void info(String str) {
		notify(generLog(INFO, str));
	}

	public void debug(String str) {
		notify(generLog(DEBUG, str));
	}
	
	Log generLog(int level, String str) {
	  Log newLog = new Log(level, str, clazz);
	  pooledLogs.add(newLog);
	  return newLog;
	}

	public void notify(Log log) {
		for (ILogRegistered regist : registered) {
			regist.logAdded(log);
		}
	}
	
	public static void register(ILogRegistered inst) {
		registered.add(inst);
		fillNewLogFollower(inst);
	}
	
	public static void unregister(ILogRegistered inst) {
		registered.remove(inst);
	}
	
	public static void fillNewLogFollower(ILogRegistered inst) {
	  for (Log log : pooledLogs) {
	    inst.logAdded(log);
	  }
	}

}

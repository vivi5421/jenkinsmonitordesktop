package org.vivi.jmd.notify;

import java.util.HashSet;
import java.util.Set;

import org.vivi.jmd.log.LogManager;
import org.vivi.jmd.log.Logger;


/**
 * Objective of this class is to control notification system. In Case you have
 * any change in what the Controler handle (feeds mainly), we have to notify
 * registered instances to let them updating their content regarding those
 * changes.
 * 
 * @author vivi
 * 
 */
public class ControlerNotifier {

	private static final transient Logger sLogger = LogManager.getLogger(ControlerNotifier.class);

	private Set<INotified> notifieds;

	public ControlerNotifier() {
		notifieds = new HashSet<INotified>();
	}

	public void registerToNotifications(INotified item) {
		notifieds.add(item);
		if (sLogger.isInfoEnabled()) {
			sLogger.info("Now registered to controler notifications [" + item.toString() + "].");
		}
	}

	public void unregisterToNotifications(INotified item) {
		notifieds.remove(item);
		if (sLogger.isInfoEnabled()) {
			sLogger.info("Now unregistered to controler notifications [" + item.toString() + "].");
		}
	}

	public void notifyRegistereds(NotifiedChanges changes) {
		if (sLogger.isInfoEnabled()) {
			sLogger.info("Notify changes to registered processes.\nRegistered processes:" + notifieds.toString()
					+ "\nChanges:\n" + changes.toString() + ".");
		}
		for (INotified notified : notifieds) {
			notified.changeOccurred(changes);
		}
		changes.clearAll();
	}

}

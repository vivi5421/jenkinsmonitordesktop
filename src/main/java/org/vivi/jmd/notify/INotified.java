package org.vivi.jmd.notify;

/**
 * Any class which wants to be notified in case of change in the controler needs
 * to implement this interface. The class will be called in case of change in
 * the controler.
 * 
 * @author vivi
 * 
 */
public interface INotified {
	
	public void changeOccurred(NotifiedChanges changes);

}

package org.vivi.jmd.notify;

/**
 * Interface to be implemented by any Class able to notfy any other regarding
 * any change in the content of the application.
 * 
 * @author vivi
 * 
 */
public interface INotifier {

	public void registerToNotifications(INotified notified);

	public void unregisterToNotifications(INotified notified);

}

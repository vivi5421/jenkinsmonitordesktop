package org.vivi.jmd.notify;

import java.util.ArrayList;
import java.util.List;

import org.vivi.jmd.log.LogManager;
import org.vivi.jmd.log.Logger;
import org.vivi.jmd.rss.model.Feed;
import org.vivi.jmd.ui.rss.JFeed;

public class NotifiedChanges {

	private static final transient Logger sLogger = LogManager.getLogger(NotifiedChanges.class);

	List<Feed> addedFeeds;

	List<Feed> updatedFeeds;

	List<Feed> updatedWithNewStatusFeeds;

	List<Feed> errorFeeds;

	List<Feed> removedFeeds;

	public NotifiedChanges() {
		addedFeeds = new ArrayList<Feed>();
		updatedFeeds = new ArrayList<Feed>();
		updatedWithNewStatusFeeds = new ArrayList<Feed>();
		errorFeeds = new ArrayList<Feed>();
		removedFeeds = new ArrayList<Feed>();
		clearAll();
	}

	public void clearAll() {
		if (sLogger.isInfoEnabled()) {
			sLogger.info("We clear changes container.");
		}
		addedFeeds.clear();
		updatedFeeds.clear();
		updatedWithNewStatusFeeds.clear();
		errorFeeds.clear();
		removedFeeds.clear();
	}

	public void checkListEmptyLog(List<Feed> list, String listName) {
		if (!list.isEmpty()) {
			sLogger.warn(listName + " is not empty: " + list.toString());
		}
	}

	public void addNewStatus(Feed feed) {
		if (sLogger.isInfoEnabled()) {
			sLogger.info("new feed in changes [newStatus] : " + feed);
		}
		updatedWithNewStatusFeeds.add(feed);
	}

	public void addUpdates(Feed feed) {
		if (sLogger.isInfoEnabled()) {
			sLogger.info("new feed in changes [update] : " + feed);
		}
		updatedFeeds.add(feed);
	}

	public void addError(Feed feed) {
		if (sLogger.isWarnEnabled()) {
			sLogger.warn("new feed in changes [error] : " + feed);
		}
		errorFeeds.add(feed);
	}

	public void addRemovedFeed(Feed feed) {
		if (sLogger.isInfoEnabled()) {
			sLogger.info("new feed in changes [remove] : " + feed);
		}
		removedFeeds.add(feed);
	}

	public void addNewFeed(Feed feed) {
		if (sLogger.isInfoEnabled()) {
			sLogger.info("new feed in changes [new] : " + feed);
		}
		addedFeeds.add(feed);
	}

	/**
	 * @return the addedFeeds
	 */
	public List<Feed> getAddedFeeds() {
		return addedFeeds;
	}

	/**
	 * @return the updatedFeeds
	 */
	public List<Feed> getUpdatedFeeds() {
		return updatedFeeds;
	}

	/**
	 * @return the updatedWithNewStatusFeeds
	 */
	public List<Feed> getUpdatedWithNewStatusFeeds() {
		return updatedWithNewStatusFeeds;
	}

	/**
	 * @return the errorFeeds
	 */
	public List<Feed> getErrorFeeds() {
		return errorFeeds;
	}

	/**
	 * @return the removedFeeds
	 */
	public List<Feed> getRemovedFeeds() {
		return removedFeeds;
	}

	public List<JFeed> getJFeeds(List<Feed> feeds) {
		List<JFeed> jFeeds = null;
		if (feeds != null) {
			jFeeds = new ArrayList<JFeed>();
			for (Feed feed : feeds) {
				jFeeds.add(new JFeed(feed));
			}
		}
		return jFeeds;
	}

	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		str.append("added=" + addedFeeds + "\n");
		str.append("updated=" + updatedFeeds + "\n");
		str.append("newStatus=" + updatedWithNewStatusFeeds + "\n");
		str.append("error=" + errorFeeds + "\n");
		str.append("removed=" + removedFeeds);
		return str.toString();
	}

}

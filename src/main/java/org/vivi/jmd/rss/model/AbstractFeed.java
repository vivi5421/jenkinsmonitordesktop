package org.vivi.jmd.rss.model;

public class AbstractFeed {

	String title;

	String link;

	JenkinsStatus status;

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the link
	 */
	public String getLink() {
		return link;
	}

	/**
	 * @param link
	 *            the link to set
	 */
	public void setLink(String link) {
		this.link = link;
	}

	/**
	 * @return the status
	 */
	public JenkinsStatus getStatus() {
		return status;
	}

	@Override
	public String toString() {
		return "title" + title + ", link=" + link + ", status=" + status;
	}

}

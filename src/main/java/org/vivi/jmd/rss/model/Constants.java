package org.vivi.jmd.rss.model;

public interface Constants {

	public static final String TITLE = "title";
	
	public static final String LINK = "link";
	
	public static final String ENTRY = "entry";
	
	public static final String PUBLISHED = "published";
	
}

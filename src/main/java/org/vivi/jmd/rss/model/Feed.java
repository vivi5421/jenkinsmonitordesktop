package org.vivi.jmd.rss.model;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Set;
import java.util.TreeSet;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.vivi.jmd.log.LogManager;
import org.vivi.jmd.log.Logger;
import org.vivi.jmd.rss.read.JenkinsHandlerDOM;
import org.vivi.jmd.rss.read.UpdateStatus;
import org.vivi.jmd.util.Utils;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/*
 * Stores an RSS feed
 */
public class Feed extends AbstractFeed {

	private static final transient Logger sLogger = LogManager.getLogger(Feed.class);

	URL url;

	Set<FeedMessage> entries;

	public Feed(URL url, String title) {
		this.title = title;
		this.url = url;
		this.entries = new TreeSet<FeedMessage>(new FeedMessageComparator());
		if (sLogger.isInfoEnabled()) {
			sLogger.info("new Feed: " + this);
		}
	}

	public UpdateStatus update() {
		UpdateStatus update = null;
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(getRSSContentInput());
			doc.getDocumentElement().normalize();
			new JenkinsHandlerDOM(this).parse(doc);
			update = updateFeedStatus();
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
			update = UpdateStatus.Error;
		}
		if (sLogger.isInfoEnabled()) {
			sLogger.info("Feed [" + this + "] has been updated and returns updateStatus [" + update + "].");
		}
		return update;
	}

	UpdateStatus updateFeedStatus() {
		UpdateStatus update = UpdateStatus.UpdatedSameStatus;
		FeedMessage feedMsg = getEntries().iterator().next();
		JenkinsStatus newStatus = feedMsg.getStatus();
		if (!Utils.areEquals(this.status, newStatus)) {
			update = UpdateStatus.UpdatedNewStatus;
		}
		if (sLogger.isDebugEnabled()) {
			String log = "updateFeedStatus is called with old value [{0}] and new value [{1}] (taken from [Feed: {2} / Message: {3}";
			String statusString = this.status != null ? this.status.name() : "null";
			sLogger.debug(Utils.generString(log, statusString, newStatus.name(), this.toString(), feedMsg.toString()));
		}
		this.status = newStatus;
		return update;
	}

	InputStream getRSSContentInput() throws IOException {
		return url.openStream();
	}

	public void addEntry(FeedMessage msg) {
		if (sLogger.isDebugEnabled()) {
			sLogger.debug("Add one entry in feed [" + this + "]: [" + msg.toString() + "].");
		}
		entries.add(msg);
	}

	public Set<FeedMessage> getEntries() {
		return entries;
	}

	/**
	 * @return the feedtitle
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the feedTitle to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the link
	 */
	public String getLink() {
		return link;
	}

	/**
	 * @param link
	 *            the link to set
	 */
	public void setLink(String link) {
		this.link = link;
	}

	/**
	 * @return the url
	 */
	public URL getUrl() {
		return url;
	}

	/**
	 * @return the status
	 */
	public JenkinsStatus getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(JenkinsStatus status) {
		this.status = status;
	}

	@Override
	public int hashCode() {
		return title.length() + url.toString().length();
	}

	@Override
	public boolean equals(Object obj) {
		boolean result = false;
		if (obj instanceof Feed) {
			Feed feed = (Feed) obj;
			result = Utils.areEquals(title, feed.getTitle()) && Utils.areEquals(url, feed.getUrl());
		}
		return result;
	}

	@Override
	public String toString() {
		return "Feed[" + super.toString() + ", url=" + url + "]";
	}
}
package org.vivi.jmd.rss.model;

import java.text.ParseException;
import java.util.Date;

import org.vivi.jmd.log.LogManager;
import org.vivi.jmd.log.Logger;
import org.vivi.jmd.util.Utils;

/*
 * Represents one RSS message
 */
public class FeedMessage extends AbstractFeed {

	private static final transient Logger sLogger = LogManager.getLogger(FeedMessage.class);

	Date published;

	public FeedMessage(String title, String link, String pubDate) throws ParseException, UnknownJenkinsStatusException {
		this.title = title;
		this.link = link;
		this.published = Utils.readDate(pubDate);
		this.status = JenkinsStatus.getStatus(title);
		if (sLogger.isDebugEnabled()) {
			sLogger.debug("new FeedMessage: " + this);
		}
	}

	@Override
	public int hashCode() {
		return (int) published.getTime();
	}

	@Override
	public boolean equals(Object obj) {
		boolean areEquals;
		if (obj instanceof FeedMessage) {
			FeedMessage fm = (FeedMessage) obj;
			areEquals = Utils.areEquals(this.title, fm.title) && Utils.areEquals(this.link, fm.link)
					&& Utils.areEquals(this.published, fm.published);
		} else {
			areEquals = false;
		}
		return areEquals;
	}

	/**
	 * @return the published
	 */
	public Date getPublished() {
		return published;
	}

	/**
	 * @param published
	 *            the published to set
	 */
	public void setPublished(Date published) {
		this.published = published;
	}

	@Override
	public String toString() {
		return "FeedMessage[" + super.toString() + ", published=" + published.toGMTString() + "]";
	}

}
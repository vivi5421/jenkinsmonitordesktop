package org.vivi.jmd.rss.model;

import java.util.Comparator;
import java.util.Date;

import org.vivi.jmd.log.LogManager;
import org.vivi.jmd.log.Logger;
import org.vivi.jmd.util.Utils;

public class FeedMessageComparator implements Comparator<FeedMessage> {

	private static final transient Logger sLogger = LogManager.getLogger(FeedMessageComparator.class);

	@Override
	public int compare(FeedMessage arg0, FeedMessage arg1) {
		int result;
		Date d0 = arg0.getPublished();
		Date d1 = arg1.getPublished();
		if (d0.after(d1)) {
			result = -1;
		} else if (d0.before(d1)) {
			result = 1;
		} else {
			result = 0;
		}
		if (sLogger.isDebugEnabled()) {
			sLogger.debug(Utils.generString("\t\t\t\tCompare [{0}][{1}]: {2}", arg0.toString(), arg1.toString(),
					String.valueOf(result)));
		}
		return result;
	}

}

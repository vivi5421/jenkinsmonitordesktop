package org.vivi.jmd.rss.model;

import org.vivi.jmd.log.LogManager;
import org.vivi.jmd.log.Logger;
import org.vivi.jmd.util.Utils;

public enum JenkinsStatus {

	Success(".*\\((stable|back to normal)\\)$"),

	Unstable(".*\\(.*(fail|failure|failures|failing.*)\\)$"),

	Failure(".*\\(broken .*\\)$");

	private static final transient Logger sLogger = LogManager.getLogger(JenkinsStatus.class);

	private String regexp;

	private JenkinsStatus(String regexp) {
		this.regexp = regexp;
	}

	public static JenkinsStatus getStatus(String text) throws UnknownJenkinsStatusException {
		for (JenkinsStatus status : JenkinsStatus.values()) {
			if (Utils.formatString(text).matches(status.regexp)) {
				if (sLogger.isDebugEnabled()) {
					sLogger.debug("Title [" + text + "] mapped to [" + status + "] Jenkins status.");
				}
				return status;
			}
		}
		if (sLogger.isWarnEnabled()) {
			sLogger.warn("This FeedMessage is not considered with any Jenkins status [" + text + "].");
		}
		throw new UnknownJenkinsStatusException(text);
	}

}

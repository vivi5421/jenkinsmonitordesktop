package org.vivi.jmd.rss.model;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class MockFeed extends Feed {
	
	String serieName;
	
	int serieIndex;

	public MockFeed(URL url, String title, String serieName, int serieIndex) {
		super(url, title);
		this.serieName = serieName;
		this.serieIndex = serieIndex;
	}
	
	@Override
	InputStream getRSSContentInput() throws IOException {
		if (serieName == null) {
		return url.openStream();
		}
		else {
			// TODO handle updates
			return null;
		}
	}
}
/**
 * 
 */
package org.vivi.jmd.rss.model;

/**
 * @author vivi
 * 
 */
public class UnknownJenkinsStatusException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String failedTitle;
	
	public UnknownJenkinsStatusException(String text) {
		this.failedTitle = text;
	}
	
	@Override
	public String getMessage() {
		return "Cannot determine Jenkins status for following title [" + failedTitle + "]";
	}

}

package org.vivi.jmd.rss.read;

import java.text.ParseException;

import org.vivi.jmd.log.LogManager;
import org.vivi.jmd.log.Logger;
import org.vivi.jmd.rss.model.Feed;
import org.vivi.jmd.rss.model.FeedMessage;
import org.vivi.jmd.rss.model.UnknownJenkinsStatusException;
import org.vivi.jmd.util.Utils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class JenkinsHandlerDOM {

	private static final transient Logger sLogger = LogManager.getLogger(JenkinsHandlerDOM.class);
	
	private Feed feed;

	public JenkinsHandlerDOM(Feed feed) {
		this.feed = feed;
	}

	public void parse(Document doc) {
		if (sLogger.isDebugEnabled()) {
			sLogger.debug("Parsing this document:\n"+Utils.readXMLDocument(doc));
		}
		parseFeed(doc.getDocumentElement());
		NodeList entries = doc.getElementsByTagName("entry");
		resetEntries();
		if (entries != null) {
			for (int i = 0; i < entries.getLength(); i++) {
				parseEntry(entries.item(i));
			}
		}
	}

	void resetEntries() {
		feed.getEntries().clear();
	}

	private void parseFeed(Element documentElement) {
		ThreeStrings map = parseElement(documentElement);
		if (feed.getTitle() == null) {
			feed.setTitle(map.title);
		}
		if (feed.getLink() == null) {
			feed.setLink(map.link);
		}
	}

	void parseEntry(Node item) {
		Element entry = (Element) item;
		ThreeStrings map = parseElement(entry);
		FeedMessage msg;
		try {
			msg = new FeedMessage(map.title, map.link, map.published);
			feed.addEntry(msg);
		} catch (ParseException | UnknownJenkinsStatusException e) {
			System.out.println(Utils.generString(
					"Could not generate this FeedMessage [{0}-{1}-{2}] for this reason:\n{3}.", map.title, map.link,
					map.published, e.getMessage()));
		}
	}

	ThreeStrings parseElement(Element element) {
		String title = element.getElementsByTagName("title").item(0).getTextContent();
		String link = ((Element) element.getElementsByTagName("link").item(0)).getAttribute("href");
		String published = element.getElementsByTagName("published").item(0).getTextContent();
		return new ThreeStrings(title, link, published);
	}

	private class ThreeStrings {

		private String title;

		private String link;

		private String published;

		private ThreeStrings(String title, String link, String published) {
			this.title = title;
			this.link = link;
			this.published = published;
		}
	}

}

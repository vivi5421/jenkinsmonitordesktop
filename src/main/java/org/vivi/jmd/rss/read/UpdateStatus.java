package org.vivi.jmd.rss.read;

public enum UpdateStatus {

	UpdatedNewStatus,

	UpdatedSameStatus,

	NoUpdate,

	Error;

}

/**
 * 
 */
package org.vivi.jmd.save;

import org.vivi.jmd.rss.model.Feed;
import org.vivi.jmd.save.model.SFeed;

/**
 * @author nvillard
 *
 */
public class FeedToSFeedAdaptor {

  public SFeed adapt(Feed feed) {
    SFeed sFeed = null;
    if (feed != null) {
      sFeed = new SFeed();
      sFeed.title = feed.getTitle();
      sFeed.url = feed.getUrl().toString();
    }
    return sFeed;
  }

}

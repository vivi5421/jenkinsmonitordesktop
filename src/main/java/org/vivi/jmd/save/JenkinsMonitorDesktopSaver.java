/**
 * 
 */
package org.vivi.jmd.save;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.prefs.Preferences;

import org.vivi.jmd.Controler;
import org.vivi.jmd.log.LogManager;
import org.vivi.jmd.log.Logger;
import org.vivi.jmd.rss.model.Feed;
import org.vivi.jmd.save.model.JenkinsMonitorDesktopOptions;
import org.vivi.jmd.save.model.SFeed;

import com.thoughtworks.xstream.XStream;

/**
 * @author nvillard
 * 
 */
public class JenkinsMonitorDesktopSaver {

	private static final transient Logger sLogger = LogManager.getLogger(JenkinsMonitorDesktopSaver.class);

	static transient XStream xstream = new XStream();

	File saveFile;

	public void save(List<Feed> feeds) throws IOException {
		JenkinsMonitorDesktopOptions options = fillOptionsFile(feeds);
		if (sLogger.isInfoEnabled()) {
			sLogger.info("Saved options:\n" + xstream.toXML(options));
			if (sLogger.isDebugEnabled()) {
				sLogger.debug("Saved content:\n" + xstream.toXML(options));
			}
		}
		xstream.toXML(options, new FileWriter(getOptionsFile()));
	}

	public List<SFeed> load(Controler controler) {
		File file = getOptionsFile();
		if (file.exists()) {
			JenkinsMonitorDesktopOptions options = (JenkinsMonitorDesktopOptions) xstream.fromXML(file);
			if (sLogger.isInfoEnabled()) {
				sLogger.info("Retrieve options");
				if (sLogger.isDebugEnabled()) {
					sLogger.debug("Retrieved content:\n" + options.toString());
				}
			}
			return options.getsFeeds();
		} else {
			if (sLogger.isWarnEnabled()) {
				sLogger.warn("File [" + file + "] does not exist");
			}
			return null;
		}
	}

	JenkinsMonitorDesktopOptions fillOptionsFile(List<Feed> feeds) {
		JenkinsMonitorDesktopOptions options = new JenkinsMonitorDesktopOptions();
		if (feeds != null) {
			options.setsFeeds(new ArrayList<SFeed>());
			FeedToSFeedAdaptor adaptor = new FeedToSFeedAdaptor();
			for (Feed feed : feeds) {
				SFeed sFeed = adaptor.adapt(feed);
				options.getsFeeds().add(sFeed);
			}
		}
		return options;
	}

	File getOptionsFile() {
		if (saveFile == null) {
			String homeFolder = System.getProperty("user.home");
			if (sLogger.isDebugEnabled()) {
				sLogger.debug("user.home variable=" + homeFolder);
			}
			Preferences systemPrefs = Preferences.userRoot().node("JenkinsMonitorDesktop");
			String optionsFilePath = systemPrefs.get("optionsFilePath", homeFolder);
			if (sLogger.isDebugEnabled()) {
				sLogger.debug("retrivingPathFileFromPreferences returns " + optionsFilePath);
			}
			saveFile = new File(optionsFilePath + File.separator + "JenkinsMonitorDesktopOptions.xml");
			if (sLogger.isInfoEnabled()) {
				sLogger.info("We will use this preferences file [" + saveFile + "].");
			}
		}
		return saveFile;
	}
}

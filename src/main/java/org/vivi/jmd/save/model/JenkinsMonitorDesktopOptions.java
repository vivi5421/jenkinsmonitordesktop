package org.vivi.jmd.save.model;

import java.io.Serializable;
import java.util.List;

public class JenkinsMonitorDesktopOptions implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	List<SFeed> sFeeds;

	/**
	 * @return the sFeeds
	 */
	public List<SFeed> getsFeeds() {
		return sFeeds;
	}

	/**
	 * @param sFeeds the sFeeds to set
	 */
	public void setsFeeds(List<SFeed> sFeeds) {
		this.sFeeds = sFeeds;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "JenkinsMonitorDesktopOptions [sFeeds=" + sFeeds + "]";
	}
	
	

}

package org.vivi.jmd.save.model;

import java.io.Serializable;

public class SFeed implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public String url;

	public String title;

	public SFeedRegexp regexp;

	@Override
	public String toString() {
		return "SFeed[url=" + url + ", title=" + title + (regexp != null ? (", " + regexp.toString()) : "") + "]";
	}
}

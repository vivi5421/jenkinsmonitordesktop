/**
 * 
 */
package org.vivi.jmd.save.model;

import java.io.Serializable;

/**
 * @author nvillard
 * 
 */
public class SFeedRegexp implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public String tagsRegexp;

	public boolean isIncludeRegexp;

}

package org.vivi.jmd.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.EventQueue;
import java.text.ParseException;
import java.util.Arrays;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;

import org.vivi.jmd.Controler;
import org.vivi.jmd.MockControler;
import org.vivi.jmd.log.LogManager;
import org.vivi.jmd.log.Logger;
import org.vivi.jmd.notify.INotified;
import org.vivi.jmd.notify.NotifiedChanges;
import org.vivi.jmd.ui.components.JAddFeedButton;
import org.vivi.jmd.ui.components.JDeleteFeedButton;
import org.vivi.jmd.ui.components.JFeedList;
import org.vivi.jmd.ui.components.JFeedMessagePanel;
import org.vivi.jmd.ui.components.JJMDTray;
import org.vivi.jmd.ui.components.JLogPanel;
import org.vivi.jmd.ui.components.JModifyFeedButton;
import org.vivi.jmd.ui.components.JUpdateFeedsButton;
import org.vivi.jmd.ui.rss.JFeed;

public class JenkinsMonitorDesktop implements INotified {

	private static final transient Logger sLogger = LogManager.getLogger(JenkinsMonitorDesktop.class);

	private static final String ARG_MOCK = "-mock";

	private static final String ARG_LOG = "-log";
	
	private static final String ARG_NO_REFRESH = "-nr";
	
	private static final String ARG_LEVEL_LOG = "-d\\d";

	private Controler controler;

	private JFrame frame;
	
	private JFrame logFrame;

	private JFeedList feedList;

	private JFeedMessagePanel jFeedMessagePanel;

	static boolean isMockMode;

	static boolean isLogMode;
	
	static boolean isNoRefreshMode;
	
	static int logLevel;

	/**
	 * Launch the application.
	 */
	public static void main(final String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					checkArgs(args);
					JenkinsMonitorDesktop window = new JenkinsMonitorDesktop();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	static void checkArgs(String[] args) {
		if (args != null) {
			List<String> lArgs = Arrays.asList(args);
			for (String arg : lArgs) {
				if (arg.equals(ARG_MOCK)) {
					isMockMode = true;
				}
				else if (arg.equals(ARG_LOG)) {
					isLogMode = true;
				}
				else if (arg.equals(ARG_NO_REFRESH)) {
					isNoRefreshMode = true;
				}
				else if (arg.matches(ARG_LEVEL_LOG)) {
					logLevel = Integer.valueOf(arg.substring(arg.length()-1));
				}
			}
		}
	}

	/**
	 * Create the application.
	 */
	public JenkinsMonitorDesktop() {
		if (isLogMode) {
			LogManager.setLevel(logLevel);
		}
		sLogger.info("isMockModeEnabled? " + isMockMode);
		sLogger.info("isDevModeEnabled? " + isLogMode);
		if (isMockMode) {
			controler = new MockControler(!isNoRefreshMode);
		} else {
			controler = new Controler(!isNoRefreshMode);
		}
		controler.registerToNotifications(this);
		initialize();
		if (sLogger.isInfoEnabled()) {
			sLogger.info("JenkinsMonitorDesktop window initialized.");
			sLogger.info("Initialization of controler...");
		}
		controler.init();
	}

	/**
	 * Initialize the contents of the frame.
	 * 
	 * @throws ParseException
	 */
	void initialize() {
		JSplitPane leftRightSplit = new JSplitPane();
		leftRightSplit.setName("Feeds");
		leftRightSplit.setRightComponent(initializeFeedMessagePanel());
		leftRightSplit.setLeftComponent(initializeFeedMenuPanel());
		if (isLogMode) {
			logFrame = new JLogPanel();
		}
		frame = initializeFrame(leftRightSplit);
		new JJMDTray(this);
	}

	Component initializeFeedMenuPanel() {
		JPanel jPanelLeft = new JPanel();
		jPanelLeft.setLayout(new BorderLayout());
		feedList = new JFeedList(jFeedMessagePanel);
		JPanel menuPanel = getMenuPanel();
		JScrollPane scrollPanel = new JScrollPane(feedList);
		jPanelLeft.add(scrollPanel, BorderLayout.CENTER);
		jPanelLeft.add(menuPanel, BorderLayout.SOUTH);
		return jPanelLeft;
	}

	Container initializeFeedMessagePanel() {
		jFeedMessagePanel = new JFeedMessagePanel();
		return jFeedMessagePanel;
	}

	JFrame initializeFrame(Component comp) {
		JFrame frame = new JFrame();
		frame.setBounds(200, 200, 800, 500);
		frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		String windowTitle = this.getClass().getSimpleName();
		if (this.isMockMode) {
			windowTitle += " - Mock mode";
		}
		frame.setTitle(windowTitle);
		frame.getContentPane().add(comp);
		return frame;
	}

	JPanel getMenuPanel() {
		JPanel menuPanel = new JPanel();
		menuPanel.add(new JAddFeedButton(controler));
		menuPanel.add(new JModifyFeedButton(controler));
		menuPanel.add(new JDeleteFeedButton(controler, feedList));
		menuPanel.add(new JUpdateFeedsButton(controler));
		return menuPanel;
	}

	@Override
	public void changeOccurred(NotifiedChanges changes) {
		if (sLogger.isInfoEnabled()) {
			sLogger.info("Handling changes UI...");
			if (sLogger.isDebugEnabled()) {
				sLogger.debug("Changes to impact to UI: " + changes.toString());
			}
		}
		if (changes != null) {
			if (hasValues(changes.getAddedFeeds())) {
				List<JFeed> jFeeds = changes.getJFeeds(changes.getAddedFeeds());
				for (JFeed jFeed : jFeeds) {
					addJFeed(jFeed);
				}
			}
			if (hasValues(changes.getErrorFeeds())) {
				// TODO
			}
			if (hasValues(changes.getRemovedFeeds())) {
				List<JFeed> jFeeds = changes.getJFeeds(changes.getRemovedFeeds());
				for (JFeed jFeed : jFeeds) {
					removeJFeed(jFeed);
				}
			}
			if (hasValues(changes.getUpdatedWithNewStatusFeeds())) {
				// TODO
			}
		}
	}

	void addJFeed(JFeed jFeed) {
		if (sLogger.isDebugEnabled()) {
			sLogger.debug("Adding this JFeed in UI: " + jFeed);
		}
		feedList.addItem(jFeed);
		jFeedMessagePanel.addItem(jFeed);
	}

	void removeJFeed(JFeed jFeed) {
		if (sLogger.isDebugEnabled()) {
			sLogger.debug("Remobing this JFeed from UI: " + jFeed);
		}
		jFeedMessagePanel.removeMessageList(jFeed);
		feedList.removeItem(jFeed);
	}

	<E> boolean hasValues(List<E> list) {
		return list != null && !list.isEmpty();
	}
	
	public void setVisible(boolean isVisible) {
		frame.setVisible(isVisible);
		logFrame.setVisible(isVisible);
	}
}

/**
 *
 */
package org.vivi.jmd.ui.components;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import org.vivi.jmd.Controler;
import org.vivi.jmd.log.LogManager;
import org.vivi.jmd.log.Logger;

/**
 * This is AddFeed button class. This class is simple, simply calling a
 * JNewFlowFrame when clicked.
 * 
 * @author nvillard
 * 
 */
public class JAddFeedButton extends JJMDButton {

	private static final transient Logger sLogger = LogManager.getLogger(JAddFeedButton.class);
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JNewFlowFrame addFeedFrame;

	public JAddFeedButton(final Controler controler) {
		super(controler, "create-icon.png", "Add");
		addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
				if (sLogger.isInfoEnabled()) {
					sLogger.info("Click on Add button");
				}
				if (addFeedFrame == null) {
					addFeedFrame = new JNewFlowFrame(controler);
				}
				addFeedFrame.setVisible(true);
			}
		});
	}

}

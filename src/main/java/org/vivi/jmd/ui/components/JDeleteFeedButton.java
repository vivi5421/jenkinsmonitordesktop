package org.vivi.jmd.ui.components;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import org.vivi.jmd.Controler;
import org.vivi.jmd.log.LogManager;
import org.vivi.jmd.log.Logger;
import org.vivi.jmd.ui.rss.JFeed;

/**
 * Button handling the delete feed(s) operation.
 * 
 * @author vivi
 * 
 */
public class JDeleteFeedButton extends JJMDButton {

	private static final transient Logger sLogger = LogManager.getLogger(JDeleteFeedButton.class);

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public JDeleteFeedButton(final Controler controler, final JFeedList feedList) {
		super(controler, "delete-icon.png", "Delete");
		addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
				if (sLogger.isInfoEnabled()) {
					sLogger.info("Click on Delete button");
				}
				deleteFeeds(feedList);
			}
		});
	}

	void deleteFeeds(JFeedList feedList) {
		List<JFeed> selectedFeeds = feedList.getSelectedItems();
		if (sLogger.isInfoEnabled()) {
			sLogger.info("To be deleted feeds are followings: " + selectedFeeds + ".");
		}
		controler.removeItems(selectedFeeds);
	}

}

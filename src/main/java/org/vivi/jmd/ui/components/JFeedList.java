package org.vivi.jmd.ui.components;

import java.util.ArrayList;
import java.util.List;

import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.vivi.jmd.ValueNotFoundInListException;
import org.vivi.jmd.log.LogManager;
import org.vivi.jmd.log.Logger;
import org.vivi.jmd.ui.rss.JFeed;
import org.vivi.jmd.util.Utils;

/**
 * Class representing the list of feeds. What is this class is handling is
 * simply the click on a given item to display the right CardLayout.
 * 
 * @author vivi
 * 
 */
public class JFeedList extends JJMDList<JFeed> {

	private static final transient Logger sLogger = LogManager.getLogger(JFeedList.class);

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JFeedMessagePanel jFeedMessagePanel;

	List<String> feeds;

	public JFeedList(JFeedMessagePanel jFeedMessagePanel) {
		this.jFeedMessagePanel = jFeedMessagePanel;
		addListSelectionListener(new ListSelectionListener() {

			@Override
			public void valueChanged(ListSelectionEvent e) {
				ListSelectionModel lsm = ((JFeedList) e.getSource()).getSelectionModel();
				if (sLogger.isDebugEnabled()) {
					String log = "ValueChanged in JFeedList. MinSelectionIndex={0}\t MaxSelectionIndex={1}.";
					sLogger.debug(Utils.generString(log, String.valueOf(lsm.getMinSelectionIndex()),
							String.valueOf(lsm.getMaxSelectionIndex())));
				}
				if (lsm.getMinSelectionIndex() == lsm.getMaxSelectionIndex()) {
					if (!isNothingSelected(lsm)) {
						JFeed selectedJFeed = listModel.getElementAt(lsm.getMinSelectionIndex());
						displayRightJFeedMessagePanel(selectedJFeed);
					}
				}
			}

			/**
			 * In case nothing is selected, -1 is returned.
			 * 
			 * @param lsm
			 * @return
			 */
			boolean isNothingSelected(ListSelectionModel lsm) {
				return lsm.getMaxSelectionIndex() == -1;
			}

		});
		feeds = new ArrayList<String>();
	}

	void displayRightJFeedMessagePanel(JFeed jFeed) {
		if (sLogger.isDebugEnabled()) {
			sLogger.debug("We display this Card [" + jFeed.getTitle() + "].");
		}
		this.jFeedMessagePanel.displayCard(jFeed.getTitle());
	}

	@Override
	public void addItem(JFeed item) {
		super.addItem(item);
		if (sLogger.isInfoEnabled()) {
			sLogger.info("We add this item in JFeedList: " + item);
		}
		feeds.add(item.getTitle());
	}

	@Override
	public void removeItem(JFeed item) {
		if (sLogger.isInfoEnabled()) {
			sLogger.info("Removing this item in JFeedList [" + item.toString() + "].");
		}
		int index;
		try {
			index = Utils.getRankInList(feeds, item.getTitle());
			if (sLogger.isDebugEnabled()) {
				sLogger.debug("To remove this item, we remove entry at index [" + index + "].");
			}
			listModel.remove(index);
			revalidate();
			repaint();
		} catch (ValueNotFoundInListException e) {
			sLogger.error("Cannot delete this Feed: " + item.getTitle() + ". \nReason: "+e.getMessage());
		}

	}
}

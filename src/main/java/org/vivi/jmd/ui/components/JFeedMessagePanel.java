package org.vivi.jmd.ui.components;

import java.awt.CardLayout;
import java.awt.Component;
import java.awt.LayoutManager;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.vivi.jmd.log.LogManager;
import org.vivi.jmd.log.Logger;
import org.vivi.jmd.ui.rss.JFeed;
import org.vivi.jmd.ui.rss.JFeedMessage;

/**
 * This is main panel for JFeedMessages display. This JPanel is linked with a
 * given CardLayout, this one can not be overriden. By this way, we don't have
 * to manage 2 instances, the panel and the layout, but only one, this class
 * taking care of any synchronization required between those 2 entities.
 * 
 * @author vivi
 * 
 */
public class JFeedMessagePanel extends JPanel {

	private static final transient Logger sLogger = LogManager.getLogger(JFeedMessagePanel.class);

	Map<String, Component> components;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private CardLayout cardLayout;

	public JFeedMessagePanel() {
		super();
		components = new HashMap<String, Component>();
		this.cardLayout = new CardLayout();
		setLayout(this.cardLayout);
	}

	/**
	 * Since we want to ensure we always use the same layout manager, we always
	 * set the same CardLayout.
	 */
	@Override
	public void setLayout(LayoutManager layout) {
		super.setLayout(cardLayout);
	}

	@Override
	public CardLayout getLayout() {
		return cardLayout;
	}

	public void removeMessageList(JFeed jFeed) {
		Component comp = components.get(jFeed.getTitle());
		if (sLogger.isInfoEnabled()) {
			sLogger.info("Is removing this specific Card: " + jFeed.getTitle() + " - " + comp);
		}
		this.cardLayout.removeLayoutComponent(comp);
		components.remove(jFeed.getTitle());
	}

	/**
	 * Display a given Card in this Panel, regarding the name provided.
	 * 
	 * @param name
	 *            the key of the Card we want to display.
	 */
	public void displayCard(String name) {
		if (sLogger.isDebugEnabled()) {
			sLogger.debug("Display this card: " + name);
		}
		getLayout().show(this, name);
		// this.revalidate();
		// this.repaint();
	}

	public void addItem(JFeed jFeed) {
		if (sLogger.isInfoEnabled()) {
			sLogger.info("Create a new Card for " + jFeed);
			if (sLogger.isDebugEnabled()) {
				sLogger.debug("This new Card will contains [" + jFeed.getMessages().size() + "] messages\n"
						+ jFeed.getMessages());
			}
		}
		JFeedMessageList jFeedMessageList = new JFeedMessageList();
		JScrollPane scrollPane = new JScrollPane(jFeedMessageList);
		add(scrollPane, jFeed.getTitle());
		for (JFeedMessage jFeedMsg : jFeed.getMessages()) {
			jFeedMessageList.addItem(jFeedMsg);
		}
		if (sLogger.isInfoEnabled()) {
			sLogger.info("We add those items in JFeedMessageList: " + jFeed.getMessages());
		}
		components.put(jFeed.getTitle(), scrollPane);
	}

}

/**
 *
 */
package org.vivi.jmd.ui.components;

import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JButton;

import org.vivi.jmd.Controler;
import org.vivi.jmd.log.LogManager;
import org.vivi.jmd.log.Logger;

/**
 * Base for a Button used in this application. Used to handle in a factorized
 * way the display of the icon.
 * 
 * @author nvillard
 * 
 */
public abstract class JJMDButton extends JButton {

	private static final transient Logger sLogger = LogManager.getLogger(JJMDButton.class);

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	Controler controler;

	public JJMDButton(Controler controler, String iconName, String label) {
		super(getImageIcon(iconName));
		this.controler = controler;
	}

	static ImageIcon getImageIcon(String fileName) {
		if (sLogger.isDebugEnabled()) {
			sLogger.debug("Trying ot reach this picture [" + fileName + "] in this folder ["
					+ JJMDButton.class.getPackage() + "]");
		}
		URL imgInput = JJMDButton.class.getResource(fileName);
		ImageIcon image = new ImageIcon(imgInput);
		return image;
	}

}

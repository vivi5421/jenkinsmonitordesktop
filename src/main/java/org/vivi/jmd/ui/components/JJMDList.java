package org.vivi.jmd.ui.components;

import java.awt.Desktop;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.vivi.jmd.log.LogManager;
import org.vivi.jmd.log.Logger;
import org.vivi.jmd.ui.renderer.FeedItemRenderer;
import org.vivi.jmd.ui.rss.AbstractJFeed;

/**
 * This class is a JList with an integrated ListModel. By this way, we handle
 * those 2 items in one shot, this class handling any synchronisation between
 * those 2 models. This class is also handling the double-click on a given item,
 * to open the browser on the right url.
 * 
 * @author vivi
 * 
 * @param <E>
 */
public abstract class JJMDList<E extends AbstractJFeed> extends JList<E> {

	private static final transient Logger sLogger = LogManager.getLogger(JJMDList.class);

	private static final long serialVersionUID = 1L;

	DefaultListModel<E> listModel;

	public JJMDList() {
		super();
		listModel = new DefaultListModel<E>();
		setModel(listModel);
		setCellRenderer(new FeedItemRenderer<AbstractJFeed>());
		addMouseListener(getDoubleClickListener());
		addListSelectionListener(getListSelectionListener());
	}

	MouseListener getDoubleClickListener() {
		MouseAdapter listener = new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
				if (isDoubleClick(e)) {
					@SuppressWarnings("unchecked")
					JJMDList<AbstractJFeed> list = (JJMDList<AbstractJFeed>) e.getSource();
					List<AbstractJFeed> selectedItems = list.getSelectedItems();
					if (sLogger.isDebugEnabled()) {
						sLogger.debug("Double-click detected on a JMDList on items [" + selectedItems + "].");
					}
					if (selectedItems != null && selectedItems.size() == 1) {
						AbstractJFeed aFeed = selectedItems.get(0);
						openInBrowser(aFeed.getLink());
					}
				}
			}

			boolean isDoubleClick(MouseEvent e) {
				return e.getClickCount() == 2;
			}
		};
		return listener;
	}

	ListSelectionListener getListSelectionListener() {
		ListSelectionListener listener = new ListSelectionListener() {

			@Override
			public void valueChanged(ListSelectionEvent e) {
				List<E> selectedItems = getSelectedItems();
				if (sLogger.isDebugEnabled()) {
					sLogger.debug("Selection changed on a JMDList on items [" + selectedItems + "].");
				}
				for (E item : getItems()) {
					if (selectedItems.contains(item)) {
						item.isSelected = true;
						if (sLogger.isDebugEnabled()) {
							sLogger.debug("\titem [" + item + "]: isSelected=true.");
						}
					} else {
						item.isSelected = false;
						if (sLogger.isDebugEnabled()) {
							sLogger.debug("\titem [" + item + "]: isSelected=false.");
						}
					}
				}
			}
		};
		return listener;
	}

	void openInBrowser(String link) {
		if (sLogger.isDebugEnabled()) {
			sLogger.debug("Trying to open in browser url: " + link + "...");
		}
		if (Desktop.isDesktopSupported()) {
			try {
				Desktop.getDesktop().browse(new URI(link));
				if (sLogger.isInfoEnabled()) {
					sLogger.info("Open in browser url: " + link);
				}
			} catch (IOException | URISyntaxException e) {
				sLogger.error("Can not open browser. Reason:" + e.getMessage());
			}
		} else {
			if (sLogger.isWarnEnabled()) {
				sLogger.warn("Desktop is not supported. Can not open browser.");
			}
		}
	}

	public List<E> getSelectedItems() {
		return getSelectedValuesList();
	}

	public List<E> getItems() {
		List<E> items = new ArrayList<E>();
		for (int i = 0; i < listModel.getSize(); i++) {
			items.add(listModel.get(i));
		}
		return items;
	}

	public void addItem(E item) {
		if (sLogger.isDebugEnabled()) {
			sLogger.debug("We add this item in list: " + item);
		}
		listModel.addElement(item);
	}

	public void removeItem(E item) {
		if (sLogger.isDebugEnabled()) {
			sLogger.debug("We remove this item in list: " + item);
		}
		listModel.removeElement(item);
	}

}

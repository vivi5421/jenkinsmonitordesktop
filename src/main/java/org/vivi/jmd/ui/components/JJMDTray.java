package org.vivi.jmd.ui.components;

import java.awt.AWTException;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.Panel;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.net.URL;
import java.util.Iterator;
import java.util.List;

import javax.swing.ImageIcon;

import org.vivi.jmd.log.LogManager;
import org.vivi.jmd.log.Logger;
import org.vivi.jmd.notify.INotified;
import org.vivi.jmd.notify.NotifiedChanges;
import org.vivi.jmd.rss.model.Feed;
import org.vivi.jmd.ui.JenkinsMonitorDesktop;

public class JJMDTray implements INotified {

	private static final transient Logger sLogger = LogManager.getLogger(JJMDTray.class);

	JenkinsMonitorDesktop jmd;

	TrayIcon sysTray;

	public JJMDTray(JenkinsMonitorDesktop jmd) {
		if (sLogger.isDebugEnabled()) {
			sLogger.debug("Trying to generate SysTray object...");
		}
		this.jmd = jmd;
		sysTray = new TrayIcon(getImage(), "JenkinsMonitorDesktop", getPopupMenu());
		sysTray.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				displayJMD();
			}
		});
		try {
			SystemTray.getSystemTray().add(sysTray);
			if (sLogger.isInfoEnabled()) {
				sLogger.info("SysTray done.");
			}
		} catch (AWTException e) {
			if (sLogger.isWarnEnabled()) {
				sLogger.warn("Could not generate systray for notifications: " + e.getMessage());
			}
		}
	}

	void displayJMD() {
		this.jmd.setVisible(true);
	}

	Image getImage() {
		URL imgInput = getClass().getResource("cool-jenkins.png");
		ImageIcon defaultIcon = new ImageIcon(imgInput);
		Image img = new BufferedImage(defaultIcon.getIconWidth(), defaultIcon.getIconHeight(),
				BufferedImage.TYPE_4BYTE_ABGR);
		defaultIcon.paintIcon(new Panel(), img.getGraphics(), 0, 0);
		return img;
	}

	PopupMenu getPopupMenu() {
		PopupMenu menu = new PopupMenu();
		MenuItem show = new MenuItem("Show");
		show.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				displayJMD();
			}
		});
		MenuItem exit = new MenuItem("Exit");
		exit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		menu.add(show);
		menu.add(exit);
		menu.add("");
		return menu;
	}

	@Override
	public void changeOccurred(NotifiedChanges changes) {
		if (changes != null) {
			if (changes.getUpdatedWithNewStatusFeeds() != null && !changes.getUpdatedWithNewStatusFeeds().isEmpty()) {
				displayNotification(changes.getUpdatedWithNewStatusFeeds());
			}
		}
	}

	void displayNotification(List<Feed> feeds) {
		String title = "New status" + (feeds.size() > 1 ? "es" : "") + ":\n";
		StringBuilder msg = new StringBuilder();
		Iterator<Feed> itFeed = feeds.iterator();
		while (itFeed.hasNext()) {
			Feed feed = itFeed.next();
			msg.append(feed.getTitle());
			msg.append(" - ");
			msg.append(feed.getStatus().name());
			if (itFeed.hasNext()) {
				msg.append("\n");
			}
		}
		sysTray.displayMessage(title, msg.toString(), TrayIcon.MessageType.INFO);
	}

}

package org.vivi.jmd.ui.components;

import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultCaret;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import org.vivi.jmd.log.ILogRegistered;
import org.vivi.jmd.log.Log;
import org.vivi.jmd.log.Logger;

public class JLogPanel extends JFrame implements ILogRegistered {

	JTextPane textPane;

	StyledDocument doc;

	Style styleError;

	Style styleWarn;

	Style styleInfo;

	Style styleDebug;

	public JLogPanel() {
		textPane = new JTextPane();
		textPane.setEditable(false);
		doc = textPane.getStyledDocument();
		initStyles(textPane);
		// Those 2 lines to ensure the scroll bar is following the new data entered
		DefaultCaret caret = (DefaultCaret) textPane.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		Logger.register(this);
		JScrollPane scrollPane = new JScrollPane(textPane);
		getContentPane().add(scrollPane);
		setSize(300, 300);
		setTitle("Logs");
		setVisible(true);
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
	}

	void initStyles(JTextPane textPane) {
		styleError = textPane.addStyle("error", null);
		StyleConstants.setForeground(styleError, Color.red);
		styleWarn = textPane.addStyle("warn", null);
		StyleConstants.setForeground(styleWarn, Color.orange);
		styleInfo = textPane.addStyle("info", null);
		StyleConstants.setForeground(styleInfo, Color.black);
		styleDebug = textPane.addStyle("debug", null);
		StyleConstants.setForeground(styleDebug, Color.gray);
	}

	@Override
	public void logAdded(Log log) {
		appendToPane(doc, log);
		textPane.validate();
		textPane.repaint();
	}

	void appendToPane(StyledDocument doc, Log log) {
		Style style = getRelatedStyle(log);
		try {
			doc.insertString(doc.getLength(), log.getMessage()+"\n\n", style);
		} catch (BadLocationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private Style getRelatedStyle(Log log) {
		switch (log.getLevel()) {
		case 0:
			return styleError;
		case 1:
			return styleWarn;
		case 2:
			return styleInfo;
		case 3:
			return styleDebug;
		default:
			return styleInfo;
		}
	}
}

/**
 * 
 */
package org.vivi.jmd.ui.components;

import org.vivi.jmd.Controler;

/**
 * @author nvillard
 * 
 */
public class JModifyFeedButton extends JJMDButton {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public JModifyFeedButton(Controler controler) {
		super(controler, "view-icon.png", "Modify Feed");
	}

}

/**
 *
 */
package org.vivi.jmd.ui.components;

import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import org.vivi.jmd.Controler;
import org.vivi.jmd.log.LogManager;
import org.vivi.jmd.log.Logger;

/**
 * Window to allow user to add a new feed. Request for title and for url (RSS).
 * 
 * @author vivi
 * 
 */
public class JNewFlowFrame extends JFrame {

	private static final transient Logger sLogger = LogManager.getLogger(JNewFlowFrame.class);

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	Controler controler;

	private JTextField titleField;

	private JTextField urlField;

	public JNewFlowFrame(Controler controler) {
		super();
		this.controler = controler;
		setBounds(100, 100, 400, 100);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setTitle("Add new feed");
		setVisible(true);
		initialize();
		if (sLogger.isInfoEnabled()) {
			sLogger.info("Creation of JNewFlow Frame");
		}
	}

	void initialize() {
		getContentPane().setLayout(new GridLayout(3, 2));
		getContentPane().add(new JLabel("title"));
		titleField = new JTextField();
		getContentPane().add(titleField);
		getContentPane().add(new JLabel("url"));
		urlField = new JTextField();
		getContentPane().add(urlField);
		getContentPane().add(initializeOkButton());
		getContentPane().add(initializeCancelButton());
	}

	JButton initializeOkButton() {
		JButton okButton = new JButton("OK");
		okButton.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
				String title = titleField.getText();
				String url = urlField.getText();
				if (isValidConfig(title, url)) {
					if (sLogger.isInfoEnabled()) {
						sLogger.info("Request to add this feed. title=" + title + "\turl=" + url + ".");
					}
					controler.addFeed(title, url);
					setVisible(false);
				} else {
					if (sLogger.isWarnEnabled()) {
						sLogger.warn("Invalid add feed request. title=" + title + "\turl=" + url + ".");
					}
					displayError("Please set both");
				}
			}
		});
		return okButton;
	}

	void displayError(String string) {
		// TODO Auto-generated method stub

	}

	boolean isValidConfig(String title, String url) {
		return title != null && url != null;
	}

	JButton initializeCancelButton() {
		JButton cancelButton = new JButton("Cancel");
		cancelButton.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
				if (sLogger.isInfoEnabled()) {
					sLogger.info("Hide JNewFlow Frame");
				}
				setVisible(false);
			}
		});
		return cancelButton;
	}

	void clearFrame() {
		if (sLogger.isInfoEnabled()) {
			sLogger.info("Clear JNewFlow Frame");
		}
		if (titleField != null) {
			titleField.setText("");
		}
		if (urlField != null) {
			urlField.setText("");
		}
	}

	@Override
	public void setVisible(boolean isVisible) {
		super.setVisible(isVisible);
		if (isVisible) {
			clearFrame();
		}
	}

}

package org.vivi.jmd.ui.components;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import org.vivi.jmd.Controler;
import org.vivi.jmd.log.LogManager;
import org.vivi.jmd.log.Logger;

public class JUpdateFeedsButton extends JJMDButton {

	private static final transient Logger sLogger = LogManager.getLogger(JUpdateFeedsButton.class);

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public JUpdateFeedsButton(final Controler controler) {
		super(controler, "circulation-icon.png", "Update");
		addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
				if (sLogger.isInfoEnabled()) {
					sLogger.info("Click on update all feeds.");
				}
				controler.updateFeeds();
			}
		});
	}

}

/**
 * 
 */
package org.vivi.jmd.ui.renderer;

import java.awt.Color;
import java.awt.Component;

import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import org.vivi.jmd.ui.rss.AbstractJFeed;

/**
 * @author vivi
 * @param <E>
 * 
 */
public class FeedItemRenderer<E extends AbstractJFeed> extends JLabel implements ListCellRenderer<E> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public Component getListCellRendererComponent(JList<? extends E> arg0, E arg1, int arg2, boolean arg3, boolean arg4) {
		final AbstractJFeed aFeed = (AbstractJFeed) arg1;
		this.setText(aFeed.getTitle());
		Icon icon = aFeed.getStatus().getPicture();
		this.setIcon(icon);
		setHorizontalAlignment(LEFT);
		setOpaque(true);
		if (aFeed.isSelected) {
			setBackground(Color.lightGray);
		} else {
			setBackground(Color.white);
		}
		return this;
	}
}

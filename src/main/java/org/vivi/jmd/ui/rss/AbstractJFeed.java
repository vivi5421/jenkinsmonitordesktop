/**
 * 
 */
package org.vivi.jmd.ui.rss;

import javax.swing.JLabel;

import org.vivi.jmd.adapt.StatusAdaptor;
import org.vivi.jmd.rss.model.AbstractFeed;

/**
 * @author vivi
 * 
 */
public abstract class AbstractJFeed extends JLabel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String title;

	String link;

	JFeedMessageStatus status;

	public boolean isSelected;

	AbstractJFeed(AbstractFeed aFeed) {
		super();
		this.link = aFeed.getLink();
		this.title = aFeed.getTitle();
		this.status = StatusAdaptor.adaptStatus(aFeed.getStatus());
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the link
	 */
	public String getLink() {
		return link;
	}

	/**
	 * @param link
	 *            the link to set
	 */
	public void setLink(String link) {
		this.link = link;
	}

	/**
	 * @return the status
	 */
	public JFeedMessageStatus getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(JFeedMessageStatus status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "title=" + title + ", link=" + link + ", status=" + status + ", isSelected=" + isSelected;
	}
}

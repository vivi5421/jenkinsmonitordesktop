package org.vivi.jmd.ui.rss;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.vivi.jmd.log.LogManager;
import org.vivi.jmd.log.Logger;
import org.vivi.jmd.rss.model.Feed;
import org.vivi.jmd.rss.model.FeedMessage;
import org.vivi.jmd.util.Utils;

public class JFeed extends AbstractJFeed {

	private static final transient Logger sLogger = LogManager.getLogger(JFeed.class);

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	List<JFeedMessage> messages;

	public JFeed(Feed feed) {
		super(feed);
		this.messages = adaptFeedMessages(feed.getEntries());
		if (sLogger.isInfoEnabled()) {
			sLogger.info("new JFeed: " + this);
			if (sLogger.isDebugEnabled()) {
				sLogger.debug("with messages: " + this.messages);
			}
		}
	}

	List<JFeedMessage> adaptFeedMessages(Set<FeedMessage> messages) {
		List<JFeedMessage> jFeedMsgs = null;
		if (messages != null) {
			jFeedMsgs = new ArrayList<JFeedMessage>();
			for (FeedMessage feedMsg : messages) {
				jFeedMsgs.add(new JFeedMessage(feedMsg));
			}
		}
		return jFeedMsgs;
	}

	/**
	 * @return the messages
	 */
	public List<JFeedMessage> getMessages() {
		return messages;
	}

	/**
	 * @param messages
	 *            the messages to set
	 */
	public void setMessages(List<JFeedMessage> messages) {
		this.messages = messages;
	}

	@Override
	public int hashCode() {
		return title.length() + link.length();
	}

	@Override
	public boolean equals(Object obj) {
		boolean result = false;
		if (obj instanceof JFeed) {
			JFeed jf = (JFeed) obj;
			result = Utils.areEquals(title, jf.getTitle()) && Utils.areEquals(link, jf.getLink());
		}
		return result;
	}

	@Override
	public String toString() {
		return "JFeed [" + super.toString() + "]";
	}
}

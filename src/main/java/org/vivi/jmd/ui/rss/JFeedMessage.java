package org.vivi.jmd.ui.rss;

import java.util.Date;

import org.vivi.jmd.log.LogManager;
import org.vivi.jmd.log.Logger;
import org.vivi.jmd.rss.model.FeedMessage;

public class JFeedMessage extends AbstractJFeed {

	private static final transient Logger sLogger = LogManager.getLogger(JFeedMessage.class);

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	Date published;

	JFeedMessage(FeedMessage feedMessage) {
		super(feedMessage);
		this.published = feedMessage.getPublished();
		if (sLogger.isDebugEnabled()) {
			sLogger.debug("JFeedMessage created: " + this);
		}
	}

	/**
	 * @return the published
	 */
	public Date getPublished() {
		return published;
	}

	/**
	 * @param published
	 *            the published to set
	 */
	public void setPublished(Date published) {
		this.published = published;
	}

	@Override
	public String toString() {
		return "JFeedMessage [" + super.toString() + ", published=" + published.toGMTString() + "]";
	}

}

package org.vivi.jmd.ui.rss;

import java.awt.Image;
import java.net.URL;

import javax.swing.ImageIcon;

public enum JFeedMessageStatus {

	Blue("blueDot.png", "Success"),

	Yellow("yellowDot.png", "Unstable"),

	Red("redDot.png", "Failure");
	
	private static final int SIZE = 25;

	private ImageIcon picture;
	
	private String description;

	private JFeedMessageStatus(String pictureName, String description) {
		this.description = description;
		URL imgInput = getClass().getResource(pictureName);
		this.picture = getImageIcon(imgInput, this.description);
	}

	/**
	 * @return the picture
	 */
	public ImageIcon getPicture() {
		return picture;
	}
	
	ImageIcon getImageIcon(URL imgPath, String description) {
		ImageIcon image = new ImageIcon(imgPath);
		return new ImageIcon(image.getImage().getScaledInstance(SIZE, SIZE, Image.SCALE_SMOOTH));
	}

}

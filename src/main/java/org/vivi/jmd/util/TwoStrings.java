package org.vivi.jmd.util;

public class TwoStrings {

	public String str1;

	public String str2;

	public TwoStrings(String str1, String str2) {
		this.str1 = str1;
		this.str2 = str2;
	}

	@Override
	public String toString() {
		return "str1=" + str1 + ", str2=" + str2;
	}

	@Override
	public int hashCode() {
		int cpt1 = str1 != null ? str1.length() : 0;
		int cpt2 = str2 != null ? str2.length() : 0;
		return cpt1 + cpt2;
	}

	@Override
	public boolean equals(Object obj) {
		boolean result = false;
		if (obj instanceof TwoStrings) {
			TwoStrings ts = (TwoStrings) obj;
			result = Utils.areEquals(str1, ts.str1) && Utils.areEquals(str2, ts.str2);
		}
		return result;
	}

}

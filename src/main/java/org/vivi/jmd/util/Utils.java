package org.vivi.jmd.util;

import java.io.StringWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.vivi.jmd.ValueNotFoundInListException;
import org.vivi.jmd.log.LogManager;
import org.vivi.jmd.log.Logger;
import org.w3c.dom.Document;

/**
 * Some utils methods.
 * 
 * @author vivi
 * 
 */
public class Utils {

	private static final transient Logger sLogger = LogManager.getLogger(Utils.class);

	private static DateFormat M_SDF2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

	/**
	 * Check if 2 objects are equals, supporting null case (when simply
	 * obj1.equals(obj2) does not.
	 * 
	 * @param obj1
	 *            first object
	 * @param obj2
	 *            second object
	 * @return true if are equals, false otherwise
	 */
	public static <E> boolean areEquals(E obj1, E obj2) {
		boolean result;
		if (obj1 == null) {
			if (obj2 == null) {
				result = true;
			} else {
				result = false;
			}
		} else {
			if (obj1.equals(obj2)) {
				result = true;
			} else {
				result = false;
			}
		}
		return result;
	}

	/**
	 * Gener a String, replacing each {i} instance with args[i].
	 * 
	 * @param pattern
	 *            the main String
	 * @param args
	 *            Array of arguments to be replaced into pattern
	 * @return the String pattern with all replaced args
	 */
	public static String generString(String pattern, String... args) {
		if (args != null && pattern != null) {
			for (int i = 0; i < args.length; i++) {
				StringBuilder pat = new StringBuilder();
				pat.append('{');
				pat.append(i);
				pat.append('}');
				while (pattern.contains(pat)) {
					pattern = pattern.replace(pat, args[i]);
				}
			}
		}
		return pattern;
	}

	/**
	 * Read a Date from a Jenkins RSS feed and parse it to return a real Date
	 * object.
	 * 
	 * @param rssDate
	 * @return
	 * @throws ParseException
	 */
	public static Date readDate(String rssDate) throws ParseException {
		return M_SDF2.parse(rssDate.substring(0, rssDate.length() - 1));
	}

	public static String formatString(String title) {
		String format = title;
		if (title != null) {
			format = title.trim();
			format = format.replaceAll("\n", "");
			format = format.replaceAll("\t", "");
		}
		return format;
	}

	public static <E> int getRankInList(List<E> items, E lookForItem) throws ValueNotFoundInListException {
		for (int i = 0; i < items.size(); i++) {
			if (areEquals(lookForItem, items.get(i))) {
				return i;
			}
		}
		throw new ValueNotFoundInListException(items, lookForItem);
	}

	public static String readXMLDocument(Document doc) {
		String output = null;
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer transformer;
		try {
			transformer = tf.newTransformer();
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			StringWriter writer = new StringWriter();
			transformer.transform(new DOMSource(doc), new StreamResult(writer));
			output = writer.getBuffer().toString().replaceAll("\n|\r", "");
		} catch (TransformerException e) {
			String log = "Cannot read [{0}] file for logging, because of this issue:\n{1}";
			sLogger.error(Utils.generString(log, doc.getDocumentURI(), e.getMessage()));
		}
		return output;
	}

}

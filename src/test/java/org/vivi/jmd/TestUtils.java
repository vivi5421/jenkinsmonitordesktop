/**
 * 
 */
package org.vivi.jmd;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.text.ParseException;
import java.util.Date;

import org.junit.Test;
import org.vivi.jmd.rss.model.JenkinsStatus;
import org.vivi.jmd.util.Utils;

/**
 * @author vivi
 * 
 */
public class TestUtils {

	/**
	 * Test method for
	 * {@link org.vivi.jmd.util.Utils#areEquals(java.lang.Object, java.lang.Object)}.
	 */
	@Test
	public void testAreEqualsStringOK() {
		assertTrue(Utils.areEquals("bonjour", "bon" + "jour"));
	}

	/**
	 * Test method for
	 * {@link org.vivi.jmd.util.Utils#areEquals(java.lang.Object, java.lang.Object)}.
	 */
	@Test
	public void testAreEqualsStringKO() {
		assertFalse(Utils.areEquals("bonjour", "bonjour\t"));
	}

	/**
	 * Test method for
	 * {@link org.vivi.jmd.util.Utils#areEquals(java.lang.Object, java.lang.Object)}.
	 */
	@Test
	public void testAreEqualsJenskinsStatusOK() {
		assertTrue(Utils.areEquals(JenkinsStatus.Failure, JenkinsStatus.Failure));
	}

	/**
	 * Test method for
	 * {@link org.vivi.jmd.util.Utils#areEquals(java.lang.Object, java.lang.Object)}.
	 */
	@Test
	public void testAreEqualsJenskinsStatusKO() {
		assertFalse(Utils.areEquals(JenkinsStatus.Failure, JenkinsStatus.Success));
	}

	/**
	 * Test method for
	 * {@link org.vivi.jmd.util.Utils#areEquals(java.lang.Object, java.lang.Object)}.
	 */
	@Test
	public void testAreEqualsnullOK() {
		assertTrue(Utils.areEquals(null, null));
	}

	/**
	 * Test method for
	 * {@link org.vivi.jmd.util.Utils#areEquals(java.lang.Object, java.lang.Object)}.
	 */
	@Test
	public void testAreEqualsnullKO1() {
		assertFalse(Utils.areEquals(null, 2));
	}

	/**
	 * Test method for
	 * {@link org.vivi.jmd.util.Utils#areEquals(java.lang.Object, java.lang.Object)}.
	 */
	@Test
	public void testAreEqualsnullKO2() {
		assertFalse(Utils.areEquals(2, null));
	}

	/**
	 * Test method for
	 * {@link org.vivi.jmd.util.Utils#generString(java.lang.String, java.lang.String[])}
	 * .
	 */
	@Test
	public void testGenerString() {
		assertEquals("bonjour", Utils.generString("bon{0}ur", "jo"));
		assertEquals("bonjour", Utils.generString("b{1}j{0}r", "ou", "on"));
		assertEquals("bonjour", Utils.generString("bonjour", "jo"));
		assertEquals("bonjour", Utils.generString("b{0}jour", "on", "jo"));
		assertNull(Utils.generString(null, "jo"));
		assertEquals("bonj{1}r", Utils.generString("b{0}j{1}r", "on"));
		assertEquals("b{0}jour", Utils.formatString("b{0}jour"));
		assertNull(Utils.generString(null, null));
	}

	/**
	 * Test method for {@link org.vivi.jmd.util.Utils#readDate(java.lang.String)}.
	 * 
	 * @throws ParseException
	 */
	@Test
	public void testReadDateOK() throws ParseException {
		Date date = Utils.readDate("2014-05-22T21:02:54Z");
		// Date starts Year in 1900
		assertEquals(2014 - 1900, date.getYear());
		// Date starts Month with January = 0
		assertEquals(5 - 1, date.getMonth());
		assertEquals(22, date.getDate());
		assertEquals(21, date.getHours());
		assertEquals(2, date.getMinutes());
		assertEquals(54, date.getSeconds());
	}

	/**
	 * Test method for {@link org.vivi.jmd.util.Utils#readDate(java.lang.String)}.
	 */
	@Test
	public void testReadDateKO() {
		try {
			Utils.readDate("bonjour");
			fail("ParseException should be raised");
		} catch (ParseException e) {
		}
	}

	/**
	 * Test method for {@link Utils#formatString(String)}.
	 */
	@Test
	public void testFormatString() {
		assertEquals("bonjour", Utils.formatString(" bonj\nour\t"));
		assertEquals("bonjour", Utils.formatString("bonjour"));
		assertEquals("bonjour", Utils.formatString(" \nb\to\n\tnj\n\nour\t\n "));
		assertEquals("bonjour", Utils.formatString(" bonj\nour\t"));
		assertEquals("", Utils.formatString(""));
		assertEquals("", Utils.formatString(" "));
		assertEquals("", Utils.formatString(" \n "));
		assertEquals(null, Utils.formatString(null));
	}

}

/**
 * 
 */
package org.vivi.jmd.adapt;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.vivi.jmd.rss.model.JenkinsStatus;
import org.vivi.jmd.ui.rss.JFeedMessageStatus;

/**
 * @author vivi
 *
 */
public class TestStatusAdaptor {
	
	Map<String, Integer> map;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		map = new HashMap<String, Integer>();
		map.put("one", 1);
		map.put("two", 2);
	}

	/**
	 * Test method for {@link org.vivi.jmd.adapt.StatusAdaptor#lookInMap(java.util.Map, java.lang.Object)}.
	 */
	@Test
	public void testLookInMap() {
		assertEquals(Integer.valueOf(1), StatusAdaptor.lookInMap(map, "one"));
		assertEquals(Integer.valueOf(2), StatusAdaptor.lookInMap(map, "two"));
		assertNull(StatusAdaptor.lookInMap(map, "three"));
		assertNull(StatusAdaptor.lookInMap(null, "three"));
	}

	/**
	 * Test method for {@link org.vivi.jmd.adapt.StatusAdaptor#lookReverseInMap(java.util.Map, java.lang.Object)}.
	 */
	@Test
	public void testLookReverseInMap() {
		assertEquals("two", StatusAdaptor.lookReverseInMap(map, 2));
		assertEquals("one", StatusAdaptor.lookReverseInMap(map, 1));
		assertNull(StatusAdaptor.lookReverseInMap(map, 3));
		assertNull(StatusAdaptor.lookReverseInMap(null, 3));
	}

	/**
	 * Test method for {@link org.vivi.jmd.adapt.StatusAdaptor#adaptStatus(org.vivi.jmd.rss.model.JenkinsStatus)}.
	 */
	@Test
	public void testAdaptStatus() {
		assertEquals(JFeedMessageStatus.Blue, StatusAdaptor.adaptStatus(JenkinsStatus.Success));
		assertEquals(JFeedMessageStatus.Yellow, StatusAdaptor.adaptStatus(JenkinsStatus.Unstable));
		assertEquals(JFeedMessageStatus.Red, StatusAdaptor.adaptStatus(JenkinsStatus.Failure));
	}

}

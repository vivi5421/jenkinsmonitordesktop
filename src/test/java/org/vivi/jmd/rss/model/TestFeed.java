/**
 * 
 */
package org.vivi.jmd.rss.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.net.URL;

import org.junit.Before;
import org.junit.Test;
import org.vivi.jmd.rss.read.UpdateStatus;

/**
 * @author vivi
 * 
 */
public class TestFeed {

	URL url;

	Feed feed;
	
	Feed feedFilled;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		url = this.getClass().getResource("hotel17.1.xml");
		feed = new Feed(url, "title");
		feedFilled = new Feed(url, "title");
		feedFilled.update();
	}

	/**
	 * Test method for {@link org.vivi.jmd.rss.model.Feed#Feed(java.net.URL)}.
	 */
	@Test
	public void testFeedURL() {
		Feed feed = new Feed(url, "title");
		assertNotNull(feed);
		assertNotNull(feed.getUrl());
		assertNotNull(feed.getTitle());
		assertTrue(feed.getEntries().isEmpty());
	}

	/**
	 * Test method for
	 * {@link org.vivi.jmd.rss.model.Feed#Feed(java.net.URL, java.lang.String)}.
	 */
	@Test
	public void testFeedURLString() {
		Feed feed = new Feed(url, "title");
		assertNotNull(feed);
		assertNotNull(feed.getUrl());
		assertEquals("title", feed.getTitle());
		assertTrue(feed.getEntries().isEmpty());
	}

	/**
	 * Test method for {@link org.vivi.jmd.rss.model.Feed#update()}.
	 */
	@Test
	public void testUpdate() {
		UpdateStatus update = feed.update();
		assertNotNull(update);
		assertEquals("title", feed.getTitle());
		assertEquals("http://jcpintegration.nce.amadeus.net/job/17.1/job/ci/job/jcp_be_unit-tests/job/hotel/", feed.getLink());
		assertTrue(!feed.getEntries().isEmpty());
	}

	/**
	 * Test method for {@link org.vivi.jmd.rss.model.Feed#updateFeedStatus()}.
	 */
	@Test
	public void testUpdateFeedStatusNewStatus() {
		feedFilled.status = JenkinsStatus.Failure;
		assertEquals(UpdateStatus.UpdatedNewStatus, feedFilled.updateFeedStatus());
	}
	
	/**
	 * Test method for {@link org.vivi.jmd.rss.model.Feed#updateFeedStatus()}.
	 */
	@Test
	public void testUpdateFeedStatusSameStatus() {
		feed.status = JenkinsStatus.Success;
		assertEquals(UpdateStatus.UpdatedSameStatus, feedFilled.updateFeedStatus());
	}

}

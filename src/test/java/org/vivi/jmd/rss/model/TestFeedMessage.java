/**
 * 
 */
package org.vivi.jmd.rss.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.vivi.jmd.util.Utils;

/**
 * @author vivi
 *
 */
public class TestFeedMessage {
	
	FeedMessage msg1;
	FeedMessage msg1bis;
	FeedMessage msg2;
	FeedMessage msg2OtherDate;
	FeedMessage msg3sameDate2;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		msg1 = new FeedMessage("17.2 » ci » jcp-be_sync_in #439 (broken since build #425)", "link", "2014-10-10T10:10:10Z");
		msg1bis = new FeedMessage("17.2 » ci » jcp-be_sync_in #439 (broken since build #425)", "link", "2014-10-10T10:10:10Z");
		msg2 = new FeedMessage("17.2 » ci » jcp-be_sync_in #440 (stable)", "link", "2014-10-10T14:23:17Z");
		msg2OtherDate = new FeedMessage("17.2 » ci » jcp-be_sync_in #440 (stable)", "link", "2014-10-10T14:23:18Z");
		msg3sameDate2 = new FeedMessage("17.2 » ci » jcp-be_sync_in #440 (stable)", "otherLink", "2014-10-10T14:23:17Z");
	}

	/**
	 * Test method for {@link org.vivi.jmd.rss.model.FeedMessage#hashCode()}.
	 */
	@Test
	public void testHashCode() {
		assertEquals(msg1.hashCode(), msg1bis.hashCode());
		assertNotEquals(msg1.hashCode(), msg2.hashCode());
		assertEquals(msg2.hashCode(), msg3sameDate2.hashCode());
	}

	/**
	 * Test method for {@link org.vivi.jmd.rss.model.FeedMessage#equals(java.lang.Object)}.
	 */
	@Test
	public void testEqualsObject() {
		assertTrue(msg1.equals(msg1bis));
		assertFalse(msg1.equals(msg2));
		assertFalse(msg1.equals(msg3sameDate2));
		assertFalse(msg2.equals(msg3sameDate2));
		assertFalse(msg1.equals("bonjour"));
		assertFalse(msg2.equals(msg2OtherDate));
	}

	/**
	 * Test method for {@link org.vivi.jmd.rss.model.FeedMessage#getStatus()}.
	 */
	@Test
	public void testGetStatus() {
		assertNotNull(msg1.getStatus());
		assertNotNull(msg1bis.getStatus());
		assertNotNull(msg2.getStatus());
		assertNotNull(msg3sameDate2.getStatus());
	}
	
	void assertNotEquals(int hashCode, int hashCode2) {
		assertFalse(Utils.areEquals(hashCode, hashCode2));
	}

}

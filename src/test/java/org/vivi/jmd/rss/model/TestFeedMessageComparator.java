/**
 * 
 */
package org.vivi.jmd.rss.model;

import static org.junit.Assert.assertEquals;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

/**
 * @author vivi
 *
 */
public class TestFeedMessageComparator {
	
	FeedMessageComparator comparator;
	
	FeedMessage old;
	
	FeedMessage recent;
	
	FeedMessage veryRecent;
	
	@Before
	public void setUp() throws ParseException, UnknownJenkinsStatusException {
		comparator = new FeedMessageComparator();
		old = new FeedMessage("(stable)", "old", "2014-10-10T10:10:10Z");
		recent = new FeedMessage("(stable)", "recent", "2014-10-11T10:10:10Z");
		veryRecent = new FeedMessage("(stable)", "veryRecent", "2014-10-11T10:10:11Z");
	}

	/**
	 * Test method for {@link org.vivi.jmd.rss.model.FeedMessageComparator#compare(org.vivi.jmd.rss.model.FeedMessage, org.vivi.jmd.rss.model.FeedMessage)}.
	 */
	@Test
	public void testCompare() {
		assertEquals(0, comparator.compare(recent, recent));
		assertEquals(1, comparator.compare(old, recent));
		assertEquals(-1, comparator.compare(recent, old));
		assertEquals(1, comparator.compare(recent, veryRecent));
		assertEquals(-1, comparator.compare(veryRecent, recent));
		assertEquals(1, comparator.compare(old, veryRecent));
		assertEquals(-1, comparator.compare(veryRecent, old));
	}
	
	@Test
	public void testSort() {
		List<FeedMessage> list = new ArrayList<FeedMessage>(Arrays.asList(recent, old, veryRecent));
		Collections.sort(list, comparator);
		assertEquals("we get "+list.get(0).getLink(), veryRecent, list.get(0));
		assertEquals("we get "+list.get(1).getLink(), recent, list.get(1));
		assertEquals("we get "+list.get(2).getLink(), old, list.get(2));
	}

}

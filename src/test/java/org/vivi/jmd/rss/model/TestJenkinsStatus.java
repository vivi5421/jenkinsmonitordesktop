/**
 * 
 */
package org.vivi.jmd.rss.model;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author vivi
 * 
 */
public class TestJenkinsStatus {

	/**
	 * Test method for
	 * {@link org.vivi.jmd.rss.model.JenkinsStatus#getStatus(java.lang.String)}.
	 * 
	 * @throws Throwable
	 */
	@Test
	public void testGetStatus() throws Throwable {
		assertEquals(JenkinsStatus.Failure,
				JenkinsStatus.getStatus("17.2 » ci » jcp-be_sync_in #439 (broken since build #425)"));
		assertEquals(JenkinsStatus.Failure,
				JenkinsStatus.getStatus("17.2 » ci » jcp-be_sync_in #425 (broken since this build)"));
		assertEquals(JenkinsStatus.Success, JenkinsStatus.getStatus("17.2 » ci » jcp-be_sync_in #424 (stable)"));
		assertEquals(JenkinsStatus.Success,
				JenkinsStatus.getStatus("17.1 » ci » jcp_be_unit-tests » hotel #652 (stable)"));
		assertEquals(JenkinsStatus.Success,
				JenkinsStatus.getStatus("17.1 » ci » jcp_be_unit-tests » hotel #651 (back to normal)"));
		assertEquals(JenkinsStatus.Unstable,
				JenkinsStatus.getStatus("17.1 » ci » jcp_be_unit-tests » hotel #650 (3 tests are still failing)"));
		assertEquals(JenkinsStatus.Unstable,
				JenkinsStatus.getStatus("17.1 » ci » jcp_be_unit-tests » hotel #649 (3 tests started to fail)"));
		assertEquals(JenkinsStatus.Failure,
				JenkinsStatus.getStatus("17.1 » ci » jcp_be_unit-tests » hotel #648 (broken since this build)"));
		assertEquals(JenkinsStatus.Unstable,
				JenkinsStatus.getStatus("17.1 » ci » jcp_be_unit-tests » hotel #647 (3 test failures)"));
	}

	/**
	 * Test method for
	 * {@link org.vivi.jmd.rss.model.JenkinsStatus#getStatus(java.lang.String)}.
	 * 
	 * @throws Throwable
	 */
	@Test
	public void testGetStatusKO() {
		try {
			JenkinsStatus.getStatus("bonjour");
			fail("UnkownJenkinsStatusException should be thrown.");
		} catch (UnknownJenkinsStatusException e) {
		}
	}

}
